
const { response } = require('@hapi/hapi/lib/validation');
const {nanoid} = require('nanoid');
const books = require('./books');

/// add a new book
const saveBookHandler = (request,h) => {
    /// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/hasOwn
    //const checkBookOwnName = Object.hasOwn(request.payload,'name');
    const checkBookOwnName = request.payload.hasownproperty('name');

    if (!checkBookOwnName) {
        return h.response({
            statusCode: 400,
            status: 'fail',
            message: 'Gagal menambahkan buku. Mohon isi nama buku'
        }).code(400);
    }

    const {name, year, author, summary, publisher, pageCount, readPage, reading} = request.payload;

    if (readPage > pageCount) {
        return h.response({
            statusCode: 400,
            status: 'fail',
            message: 'Gagal menambahkan buku. readPage tidak boleh lebih besar dari pageCount'
        }).code(400);
    }

    const checkFinish = (pageCount,readPage) => {
        if (pageCount === readPage) {
        return true
      } return false
      };

    const id = nanoid(16);
    const finished = checkFinish(pageCount,readPage);
    const insertedAt = new Date().toISOString();
    const updatedAt = insertedAt

    const newBook = {
        id,
        name,
        year,
        author,
        summary,
        publisher,
        pageCount,
        readPage,
        finished,
        reading,
        insertedAt,
        updatedAt
    };

    books.push(newBook);

    const success = books.filter(book => book.id === id).length > 0;

    if (success) {
        return h.response({
            statusCode: 201,
            status: 'success',
            message: 'Buku berhasil ditambahkan',
            data: {
                bookId: id
            }
        }).code(201);
    }

    return h.response({
        statusCode: 500,
        status : 'fail',
        message: 'Gagal menambahkan buku'
    }).code(500);



}

const showBookHandler = (request,h) => {
    const {name, reading, finished} = request.query;

   if (!name && !reading && !finished) {
       return h.response({
           status : 'success',
           data :{
               books : books.map ((book) => ({
                   id: book.id,
                   name: book.name,
                   publisher: book.publisher
               })),
           },
       }).code(200)
   }

   if (name) {
         
    const filteredBooks = books.filter(book => book.name.toLowerCase().includes(name.toLowerCase()));
         return h.response({
              status : 'success',
              data : {
                books : filteredBooks.map ((book) => ({
                     id: book.id,
                     name: book.name,
                     publisher: book.publisher
                })),
              },
         }).code(200)
   }

   if (reading) {
         const readingBooks = books.filter(book => Number(book.reading) === Number(reading));
         return h.response({
              status : 'success',
              data : {
                books : readingBooks.map ((book) => ({
                     id: book.id,
                     name: book.name,
                     publisher: book.publisher
                })),
              },
         }).code(200)
   }
        const finishedBooks = books.filter(book => Number(book.finished) === Number(finished));
        return h.response({
             status : 'success',
             data : {
                books : finishedBooks.map ((book) => ({
                     id: book.id,
                     name: book.name,
                     publisher: book.publisher
                })),
             },
        }).code(200)
};

const detailBookHandler = (request,h) => {
    const {bookId} = request.params;

    const book = books.filter(book => book.id === bookId)[0];

    if (book) {
        return h.response({
            status : 'success',
            data : {
                book: book
            }
        }).code(200);
    } else {
    return h.response({
        statusCode: 404,
        status : 'fail',
        message : 'Buku tidak ditemukan'
    }).code(404);}
}

const updateBookHandler = (request,h) => {
    const {bookId} = request.params;

    //const checkName = Object.hasOwn(request.payload,'name');
    const checkName = request.payload.hasownproperty('name');
    const {readPage,pageCount} = request.payload;
    const checkReadpage = readPage <= pageCount;

    if (!checkName) {
        return h.response({
            statusCode: 400,
            status: 'fail',
            message: 'Gagal memperbarui buku. Mohon isi nama buku'
        }).code(400);
    } else if (!checkReadpage) {
        return h.response({
            statusCode: 400,
            status: 'fail',
            message: 'Gagal memperbarui buku. readPage tidak boleh lebih besar dari pageCount'
        }).code(400);
    } else if (checkName && checkReadpage) {
        const {bookId} = request.params;

        const {name,year,author,summary,publisher,pageCount,readPage,reading} = request.payload;
        const updatedAt = new Date().toISOString();

        const index = books.findIndex(book => book.id === bookId);

        if (index !== -1) {
            books[index] = {...books[index],
            name,
            year,
            author,
            summary,
            publisher,
            pageCount,
            readPage,
            reading,
            updatedAt
            };
            return h.response({
                statusCode: 200,
                status: 'success',
                message: 'Buku berhasil diperbarui',
            }).code(200);
        } else {
            return h.response({
                statusCode: 404,
                status: 'fail',
                message: 'Gagal memperbarui buku. Id tidak ditemukan'
            }).code(404);
        }

    }
}

const deleteBookHandler = (request,h) => {
    const {bookId} = request.params;
    const index = books.findIndex(book => book.id === bookId);

    if (index !== -1) {
        books.splice(index,1);
        return h.response ({
            statusCode: 200,
            status: 'success',
            message: 'Buku berhasil dihapus'
        }).code(200);
    } else {
        return h.response({
            statusCode: 404,
            status: 'fail',
            message: 'Buku gagal dihapus. Id tidak ditemukan'
        }).code(404);
    }
}


module.exports = {saveBookHandler, showBookHandler, detailBookHandler, updateBookHandler, deleteBookHandler};